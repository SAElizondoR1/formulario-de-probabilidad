\documentclass[]{report}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}


% Title Page
\title{Formulario de probabilidad y estadística}
\author{Sergio Andrés Elizondo Rodríguez}


\begin{document}
\maketitle

\part{Probabilidad}

\chapter{Introducción}

\section{Métodos de conteo}

\begin{itemize}
	\item Principio del producto: $ |N_1 \times N_2| = |N_1| \cdot |N_2| $
	\item Número de permutaciones de $ n $ elementos: $ P(n, n) = n! $
	\item Número de permutaciones de $ n $ elementos, tomados $ r $ a la vez: \\
	$ P(n, r) = \frac{n!}{(n - r)!}\ \forall r = 0, 1, 2, ..., n $
	\item Número de permutaciones  circulares de $ n $ elementos: $ PC_n = (n - 1)! $
\end{itemize}

\section{Combinaciones}

\begin{itemize}
	\item Número de combinaciones de $ n $ elementos, tomados $ r $ a la vez: \\
	$ C(n, r) = \binom{n}{r} = \frac{P(n, r)}{r!} = \frac{n!}{r!(n - r)!}\ \forall r = 0, 1, 2, ..., n $
	\item Número de maneras de partir un conjunto con $ n $ elementos en $ k $ subconjuntos con $n_1, n_2, ..., n_k $ elementos cada uno, respectivamente: \\
	$ \binom{n}{n_1, n_2, ..., n_k} = \frac{n!}{n_1! \cdot n_2! \cdot ... \cdot n_k!} $
	\item Fórmula de Stirling: $n! \sim \sqrt{2 \pi n} \left( \frac{n}{e} \right)^n $
\end{itemize}

\section{Coeficientes binomiales}

\begin{itemize}
	\item Teorema del binomio: $ (x + y)^n = \sum\limits_{r = 0}^n \binom{n}{r} x^{n - r} y^r\ \forall n \in \mathbb{Z}^+ $
	\item Identidad de simetría: $ \binom{n}{r} = \binom{n}{n - r}\ \forall n \in \mathbb{Z}^+ \land r = 0, 1, 2, ..., n $
	\item Regla de Jalaiuda: $ \binom{n}{r} = \binom{n - 1}{r} + \binom{n - 1}{r - 1}\ \forall n \in \mathbb{Z}^+ \land r = 0, 1, 2, ..., n $
	\item Identidad de Chu-Vandermonde: $ \sum\limits_{r = 0}^{k} \binom{m}{r} \binom{n}{k-r} = \binom{m+n}{k} $
\end{itemize}

\chapter{Probabilidad}

\section{Introducción}

\begin{itemize}
	\item Definición clásica de probabilidad: $ P(A) = \frac{n}{N} $
\end{itemize}

\section{Eventos}

\begin{itemize}
	\item Definición de unión: $ A \cup B = \{ x | x \in A \lor x \in B \}\ \forall A, B \subset S $
	\item Definición de intersección: $ A \cap B = \{ x | x \in B \and x \in B \}\ \forall A, B \subset S $
	\item Definición de complemento: $ A' = \{ x | x\in S \land x \notin A \}\ \forall A \subset S  $
	\item Leyes de De Morgan: \begin{itemize}
		\item Regla de negación de la conjunción: $ (A \cap B)' = A' \cup B' $
		\item Regla de negación de la disyunción: $ (A \cup B)' = A' \cap B' $
	\end{itemize}
\end{itemize}

\section{La probabilidad de un evento}

\begin{itemize}
	\item Probabilidad de un evento $ A $ del espacio muestral finito $ S $ con eventos equiprobables: $ P(A) = \frac{|A|}{|S|} $
	\item Postulados de Kolmogórov: \begin{enumerate}
		\item $ P(A) \ge 0\ \forall A \subset S $
		\item $ P(S) = 1 $
		\item Si los eventos $ A_1, A_2, ... \subset S $ son mutuamente excluyentes, entonces \\
		$ P(A_1 \cup A_2 \cup ...) = P(A_1) + P(A_2) + ...  $
	\end{enumerate}
\end{itemize}

\section{Algunas reglas de probabilidad}

\begin{itemize}
	\item Probabilidad del complemento: $ P(A') = 1 - P(A)\ \forall A \subset S $
	\item Probabilidad del suceso imposible: $ P(\emptyset) = 0 $
	\item Probabilidad de un subconjunto: $ A \subset B \implies P(A) \le P(B)\ \forall A, B \subset S $
	\item Imagen de la función probabilidad: $ 0 \le P(A) \le 1\ \forall A \subset S $
	\item Principio de inclusión-exclusión \begin{itemize}
		\item n = 2:  $ P(A \cup B) = P(A) + P(B) - P(A \cap B)\ \forall A, B \subset S $
		\item n = 3: $ P(A \cup B \cup C) = P(A) + P(B) + P(C) - P(A \cap B) - P(A \cap C) - P(B \cap C) + P(A \cap B \cap C)\ \forall A, B, C \subset S $
	\end{itemize}
	\item Probabilidad de un evento dada una ventaja de $ a $ a $ b$: $ p = \frac{a}{a + b} $
\end{itemize}

\section{Probabilidad condicional}

\begin{itemize}
	\item Probabilidad condicional de $ B $ dado $ A $: $ P(B | A) = \frac{P(A \cap B)}{P(A)} $
	\item Regla de la multiplicación: $ P(A \cap B) = P(A) \cdot P(B | A) $
\end{itemize}

\section{Eventos independientes}

\begin{itemize}
	\item Condición de independencia: $ P\left( \bigcap\limits_{i = 1}^k A_i \right) = \prod\limits_{i = 1}^k P(A_i) $
\end{itemize}

\section{Teorema de Bayes}

\begin{itemize}
	\item Teorema de la probabilidad total: Si los eventos $ B_1, B_2, ..., B_k $ son una partición de $ S $ y $ P(B_i) \ne 0 $ para $ i = 1, 2, ..., k $, entonces \\
	$ P(A) = \sum\limits_{i = 1}^k P(B_i) \cdot P(A | B_i)\ \forall A \subset S $
	\item Teorema de Bayes: Si los eventos $ B_1, B_2, ..., B_k $ son una partición de $ S $ y $ P(B_i) \ne 0 $ para $ i = 1, 2, ..., k $, entonces \\
	$ P(B_i | A) = \frac{P(B_i) \cdot P(A | B_i)}{\sum\limits_{i = 1}^k P(B_i) \cdot P(A | B_i)}\ \forall A \subset S $
\end{itemize}

\chapter{Distribuciones y densidades de probabilidad}

\section{Distribuciones de probabilidad}

\begin{itemize}
	\item Condiciones para que una función sea una distribución de probabilidad: \begin{enumerate}
		\item $ f(x) \ge 0\ \forall x \in Dom_f $
		\item $ \sum\limits_{x \in Dom_f} f(x) = 1 $
	\end{enumerate}
\end{itemize}

\subsection{Funciones de distribución acumulativa}

\begin{itemize}
	\item Definición de función de distribución acumulativa: \\
	$ F(x) = P(X \le x) = \sum\limits_{t \le x} f(t)\ \forall x \in (-\infty, \infty) $
	\item Propiedades de la función de distribución: \begin{enumerate}
		\item $ F(-\infty) = 0 $
		\item $ F(\infty) = 1 $
		\item $ a < b \implies F(a) \le F(b)\ \forall a, b \in \mathbb{R} $
		\item $ (Dom_f = \{ x_1, x_2, ..., x_n \} \land x_1 < x_2 < ... < x_n) \\
		\implies f(x_i) = \begin{cases}
		F(x_i) & \text{para } i = 1 \\
		F(x_i) - F(x_{i - 1}) & \text{para } i = 2, 3, ..., n
		\end{cases} $
	\end{enumerate}
\end{itemize}

\section{Funciones de densidad de probabilidad}

\begin{itemize}
	\item Definición de función de densidad de probabilidad: \\
	$ P(a \le X \le b) = \int\limits_a^b f(x) dx\ \forall a, b \in \mathbb{R} | a \le b $
	\item Si X es una variable aleatoria continua, \\
	$ (a, b \in \mathbb{R} \land a \le b) \\
	\implies P(a \le X \le b) = P(a \le X < b) = P(a < X \le b) = P(a < X < b) $
	\item Condiciones para que una función sea una densidad de probabilidad: \begin{enumerate}
		\item $ f(x) \ge 0\ \forall x \in (-\infty, \infty) $
		\item $ \int\limits_{-\infty}^{\infty} f(x) dx = 1 $
	\end{enumerate}
\end{itemize}

\subsection{Funciones de distribución acumulativa de variable aleatoria}

\begin{itemize}
	\item Definición de función de distribución acumulativa de variable aleatoria: \\
	$ F(x) = P(x \le x) = \int\limits_{-\infty}^x f(t)dt\ \forall x \in (-\infty, \infty) $
	\item Propiedades de la función de distribución de variable aleatoria: \begin{enumerate}
		\item $ P(A \le X \le b) = F(b) - F(a)\ \forall a, b \in \mathbb{R} | a \le b $
		\item $ f(x) = \frac{dF(x)}{dx} $, si la derivada existe.
	\end{enumerate}
\end{itemize}

\section{Distribuciones multivariadas}

\subsection{Distribuciones de probabilidad conjunta}

\begin{itemize}
	\item Definición de distribución de probabilidad conjunta: \\
	$ f(x, y) = P(X = x, Y = y) $
	\item Condiciones para que una función sea una distribución de probabilidad conjunta: \begin{enumerate}
		\item $ f(x, y) \ge 0\ \forall (x, y) \in Dom_f $
		\item $ \sum\limits_{x} \sum\limits_y f(x, y) = 1\ |(x, y) \in Dom_f $
	\end{enumerate}
	\item Definición de función de distribución acumulativa conjunta: \\
	$ F(x, y) = P(X \le x, Y \le y) = \sum\limits_{s \le x} \sum\limits_{t \le y} f(s, t)\ \forall x, y \in (-\infty, \infty) $
\end{itemize}

\subsection{Funciones de densidad de probabilidad conjunta}

\begin{itemize}
	\item Definición de función de densidad de probabilidad conjunta: \\
	$ P[(X, Y) \in A] = \int_A \int f(x,y) dx dy\ \forall A \subset \mathbb{R}^2 $
	\item Condiciones para que una función sea una densidad de probabilidad conjunta: \begin{enumerate}
		\item $ f(x, y) \ge 0\ \forall x, y \in (-\infty, \infty) $
		\item $ \int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} f(x, y) dx dy = 1 $
	\end{enumerate}
	\item Definición de función de distribución conjunta de variable aleatoria: \\
	$ F(x, y) = P(X \le x, Y \le y) = \int\limits_{-\infty}^y \int\limits_{-\infty}^x f(s, t) ds dt\ \forall x, y \in (-\infty, \infty) $
	\item Densidad de probabilidad conjunta dada la función de distribución conjunta: $ \frac{\partial^2}{\partial x \partial y} F(x, y) $
\end{itemize}

\section{Distribuciones marginales}

\begin{itemize}
	\item Definición de distribución marginal de X: $ g(x) = \sum\limits_y f(x, y) $
	\item Definición de distribución marginal de Y: $ h(y) = \sum\limits_x f(x, y) $
	\item Definición de densidad marginal de X: $ g(x) = \int\limits_{-\infty}^{\infty} f(x, y) dy\ \forall x \in (-\infty, \infty) $
	\item Definición de densidad marginal de Y: $ h(y) = \int\limits_{-\infty}^{\infty} f(x, y) dx\ \forall y \in (-\infty, \infty) $
\end{itemize}

\section{Distribuciones condicionales}

\begin{itemize}
	\item Distribución/densidad condicional de X: $ f(x | y) = \frac{f(x, y)}{h(y)}, h(y) \ne 0 $
	\item Distribución/densidad condicional de Y: $ w(y | x) = \frac{f(x, y)}{g(x)}, g(x) \ne 0 $
\end{itemize}

\chapter{Esperanza matemática}

\begin{itemize}
	\item Definición de valor esperado: $ E(X) = \sum\limits_x x \cdot f(x) $
	\item Definición de valor esperado de una variable continua: \\
	$ E(X) = \int\limits_{-\infty}^{\infty} x \cdot f(x) dx $
	\item Valor esperado de una función: $ E[g(X)] = \sum\limits_x g(x) \cdot f(x) $
	\item Valor esperado de una función de variable continua: \\
	$ E[g(X)] = \int\limits_{-\infty}^{\infty} g(x) \cdot f(x) dx $
	\item Valor esperado de una constante: $ E(b) = b $
	\item Valor esperado del producto: $ E(aX) = aE(X) $
	\item Valor esperado de una combinación lineal de funciones: \\
	$ E \left[ \sum\limits_{i = 1}^n c_i g_i(X) \right] = \sum\limits_{i = 1}^n c_i E[g_i(X)] $
	\item Valor esperado de una función bivariada: \\
	$ E[g(X, Y)] = \sum\limits_x \sum\limits_y g(x, y) \cdot f(x, y) $
	\item Valor esperado de una función bivariada de variable continua: \\
	$ E[g(X, Y)] = \int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} g(x, y) f(x, y) dx dy $
\end{itemize}

\section{Momentos}

\begin{itemize}
	\item R-ésimo momento alrededor del origen: \\
	$ \mu_r' = E(X^r) = \sum\limits_x x^r \cdot f(x)\ \forall r = 0, 1, 2, ... $
	\item R-ésimo momento alrededor del origen de una función de variable continua: \\
	$ \mu_r' = E(X^r) = \int\limits_{-\infty}^{\infty} x^r \cdot f(x) dx $
	\item Media: $ \mu = \mu_1' $
	\item R-ésimo momento alrededor de la media: \\
	$ \mu_r = E[(X - \mu)^r] = \sum\limits_x (x - \mu)^r \cdot f(x)\ \forall r = 0, 1, 2, ... $
	\item R-ésimo momento alrededor de la media de una función de variable continua: \\
	$  \mu_r = E[(X - \mu)^r] = \int\limits_{-\infty}^{\infty} (x - \mu)^r \cdot f(x) dx $
	\item Varianza: $ var(X) = \sigma^2 = \mu_2 = \mu_2' - \mu^2 $
	\item Desviación estándar: $ \sigma = +\sqrt{var(X)} $
	\item Varianza del producto y la suma: $ var(aX + b) = a^2 \sigma^2 $
\end{itemize}

\section{Teorema de Chebyshev}

\begin{itemize}
	\item Teorema de Chebyshev: $ P(|X - \mu| < k\sigma) \ge 1 - \frac{1}{k^2}\ \forall k \in \mathbb{R}^+ $
\end{itemize}

\section{Funciones generatrices de momentos}

\begin{itemize}
	\item Definición de función generatriz de momentos: \\
	$ M_X(t) = E(e^{tX}) = \sum\limits_x e^{tx} \cdot f(x) $
	\item Definición de función generatriz de momentos de una función de variable continua: \\
	$ M_X(t) = E(e^{tX}) = \int\limits_{-\infty}^{\infty} e^{tx} \cdot f(x) dx $
	\item R-ésimo momento alrededor del origen dada la función generatriz: \\
	$ \mu_r' = \frac{d^r M_X(t)}{dt^r} \big|_{t = 0} $
\end{itemize}

\section{Momentos producto}

\begin{itemize}
	\item Momento producto alrededor del origen: \\
	$ \mu_{r, s}' = E(X^r Y^s) = \sum\limits_x \sum\limits_y x^r y^s \cdot f(x, y)\ \forall r = 0, 1, 2, ..., s = 0, 1, 2, ... $
	\item Mmento producto alrededor del origen de una función de variable continua:  \\
	$ \mu_{r, s}' = E(X^r Y^s) = \int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} x^r y^s \cdot f(x, y) dx dy $
	\item Media de X: $ \mu_X = \mu_{1, 0}' = E(X) $
	\item Media de Y: $ \mu_Y = \mu_{0, 1}' = E(Y) $
	\item Momento producto alrededor de la media: \\
	$ \mu_{r, s} = E[(X - \mu_X)^r (Y - \mu_Y)^s] = \sum\limits_x \sum\limits_y (x - \mu_X)^r (y - \mu_Y)^s \cdot f(x, y) \\ \forall r = 0, 1, 2, ..., s = 0, 1, 2, ... $
	\item Momento producto alrededor de la media de una función de variable continua: \\
	$ \mu_{r, s} = E[(X - \mu_X)^r (Y - \mu_Y)^s] = \int\limits_{-\infty}^{\infty} \int\limits_{-\infty}^{\infty} (x - \mu_X)^r (y - \mu_Y)^s \cdot f(x, y) dx dy $
	\item Covarianza: \\
	$ cov(X, Y) = \sigma_{XY} = \mu_{1, 1} = \sum\limits_x \sum\limits_y (x - \mu_X) (x - \mu_y) f(x, y) = \mu_{1, 1}' - \mu_X \mu_Y $
\end{itemize}

\chapter{Distribuciones de probabilidad especiales}

\section{Distribución uniforme discreta}

\begin{itemize}
	\item Definición de distribución uniforme discreta: \\
	$ f(x) = \frac{1}{k}\ \forall x = x_1, x_2, ..., x_k | (i \ne j) \implies (x_i \ne x_j) $
	\item Media de la distribución uniforme discreta: $ \mu = \frac{k + 1}{2} $
	\item Varianza de la distribución uniforme discreta: $ \sigma^2 = \frac{k^2 - 1}{12} $
\end{itemize}

\section{Otras distribuciones}

\begin{itemize}
	\item Definición de distribución de Bernoulli: $ f(x; \theta) = \theta^x (1 - \theta)^{1 - x}\ \forall x = 0, 1 $
\end{itemize}

\section{Distribución binomial}

\begin{itemize}
	\item Definición de distribución binomial: \\
	$ b(x; n, \theta) = \binom{n}{x} \theta^x (1 - \theta)^{n - x}\ \forall x = 0, 1, 2, ..., n $
	\item Espacio muestral de la distribución binomial: $ \sum\limits_{x = 0}^n \binom{n}{x} \theta^x (1 - \theta)^{n - x} = 1 $
	\item Complemento de la distribución binomial: $ b(x; n, \theta) = b(n - x; n, 1 - \theta) $
	\item Media de la distribución binomial: $ \mu = n\theta $
	\item Varianza de la distribución binomial: $ \sigma^2 = n\theta(1 - \theta) $
\end{itemize}

\section{Distribución binomial negativa y geométrica}

\begin{itemize}
	\item Definición de distribución binomial negativa: \\
	$ b^*(x; k, \theta) = \binom{x - 1}{k - 1} \theta^k (1 - \theta)^{x - k}\ \forall x = k, k + 1, k + 2, ... $
	\item Definición de distribución geométrica: $ g(x; \theta) = \theta (1 - \theta)^{x - 1}\ \forall x = 1, 2, 3, ... $
\end{itemize}

\section{Distribución hipergeométrica}

\begin{itemize}
	\item Definición de distribución hipergeométrica: \\
	$ h(x; n, N, M) = \frac{\binom{M}{x} \binom{N - M}{n - x}}{\binom{N}{n}}\ \forall x = 0, 1, 2, .., n; x \le M; n - x \le N - M $
	\item Media de la distribución hipergeométrica: $ \mu = \frac{nM}{N} $
	\item Varianza de la distribución hipergeométrica: $ \frac{nM(N - M)(N - n)}{N^2 (N - 1)} $
\end{itemize}

\section{Distribución de Poisson}

\begin{itemize}
	\item Definición de distribución de Poisson: \\
	$ p(x; \lambda) = \frac{\lambda^x e^{-\lambda}}{x!}\ \forall x = 0, 1, 2, ...; \lambda = n\theta $
	\item Media de la distribución de Poisson: $ \mu = \lambda $
	\item Varianza de la distribución de Poisson: $ \sigma^2 = \lambda $
\end{itemize}

\section{Distribución multinomial}

\begin{itemize}
	\item Definición de distribución multinomial: \\
	$ f(x_1, x_2, ..., x_k; n, \theta_1, \theta_2, ..., \theta_k) = \binom{n}{x_1, x_2, ..., x_k} \cdot \theta_1^{x_1} \cdot \theta_2^{x_2} \cdot ... \cdot \theta_k^{x_k}; \\
	x_i = 0, 1, ..., n; \sum\limits_{i = 1}^k x_i = n; \sum\limits_{i = 1}^k \theta_i = 1 $
\end{itemize}

\section{Distribución hipergeométrica multivariada}

\begin{itemize}
	\item Definición de distribución hipergeométrica multivariada: \\
	$ f(x_1, x_2, ..., x_k; n, M_1, M_2, ..., M_k) = \frac{\binom{M_1}{x_1} \cdot \binom{M_2}{x_2} \cdot ... \cdot \binom{M_k}{x_k}}{\binom{N}{n}} $ \\
	para $x_i = 0, 1, ..., n$, $x_i \le M_i$, $\sum\limits_{i = 1}^{n} x_i = $n, $\sum\limits_{i = 1}^{n} M_i = N$
\end{itemize}

\chapter{Densidades de probabilidad especiales}

\section{Distribución uniforme}

\begin{itemize}
	\item Definición de distribución uniforme: \\
	$ u(x; \alpha, \beta) = \begin{cases}
		\frac{1}{\beta - \alpha} & \text{para } \alpha < x < \beta \\
		0 & \text{en cualquier otra parte}
	\end{cases} $
	\item Media de la distribución uniforme: $\frac{\alpha + \beta}{2}$
	\item Varianza de la distribución uniforme: $\frac{1}{12} (\beta - \alpha)^2$
\end{itemize}

\section{Distribución gamma, exponencial y ji cuadrada}

\begin{itemize}
	\item Definición de la función gamma: $ \Gamma(\alpha) = \int\limits_{0}^{\infty} y^{\alpha - 1} e^y dy $
	\item Definición recursiva de la función gamma: $\Gamma(\alpha) = (\alpha - 1) \cdot \Gamma(\alpha - 1)$
	\item Definición de distribución gamma: \\
	$ g(x; \alpha, \beta) = \begin{cases}
		\frac{1}{\beta^{\alpha} \Gamma(\alpha)} x^{\alpha - 1} e^{-\frac{x}{\beta}} & \text{para } x > 0 \\
		0 & \text{en culaquier otra parte}
	\end{cases} $ \\
	\text{donde $ \alpha > 0 $ y $ \beta > 0 $}
	\item R-ésimo momento alrededor del origen de la distribución gamma: \\
	$\mu_r' = \frac{\beta^r \Gamma(\alpha + r)}{\Gamma(\alpha)}$
	\item Media de la distribución gamma: $\mu = \alpha \beta$
	\item Varianza de la distribución gamma: $\sigma^2 = \alpha \beta^2$
	\item Definición de distribución exponencial: \\
	$g(x; \theta) = \begin{cases}
		\frac{1}{\theta} e^{-x / \theta} & \text{para } x > 0 \\
		0 & \text{en cualquier otra parte}
	\end{cases}$ \\
	\text{donde $ \theta > 0 $}
	\item Media de la distribución exponencial: $\mu = \theta$
	\item Varianza de la distribución exponencial: $\sigma^2 = \theta^2$
	\item Definición de distribución ji cuadrada: \\
	$f(x) = \begin{cases}
		\frac{1}{2^{\nu / 2} \Gamma(\nu / 2)} x^{\frac{\nu - 2}{2}} e^{-\frac{x}{2}} & \text{para } x > 0 \\
		0 & \text{en cualquier otra parte}
	\end{cases}$
	\item Media de la distribución ji cuadraada: $\mu = \nu$
	\item Varianza de la distribución ji cuadrada: $\sigma^2 = 2\nu$
\end{itemize}

\section{Distribución beta}

\begin{itemize}
	\item Definición de distribución beta: \\
	$f(x) = \begin{cases}
		\frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \cdot \Gamma(\beta)} x^{\alpha - 1} (1 - x)^{\beta - 1} & \text{para } 0 < x < 1 \\
		0 & \text{en cualquier otra parte}
	\end{cases}$ \\
	donde $\alpha > 0$, $\beta > 0$
	\item Media de la distribución beta: $\mu = \frac{\alpha}{\alpha + \beta}$
	\item Varianza de la distribución beta: $\sigma^2 = \frac{\alpha \beta}{(\alpha + \beta)^2 (\alpha + \beta + 1)}$
\end{itemize}

\section{Distribución normal}

\begin{itemize}
	\item Definición de distribución normal:
	$n(x; \mu, \sigma) = \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{1}{2} \left( \frac{x - \mu}{\sigma} \right)^2}\ \\
	\forall x \in (-\infty, \infty)$
	\item Función generadora de momentos de la distribución normal: \\
	$ M_X(t) = e^{\mu t + \frac{1}{2} \sigma^2 t^2} $
	\item Distribución normal estándar: $ \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{1}{2} x^2} $
	\item Variable aleatoria de la distribución normal estándar a partir de una de distribución normal: $Z = \frac{X - \mu}{\sigma}$
	\item Máximo relativo de la distribución normal: $x = \mu$
	\item Puntos de inflexión: \\
	$x = \mu - \sigma$ \\
	$x = \mu + \sigma$
\end{itemize}

\section{Aproximación normal a la distribución normal}

\begin{itemize}
	\item Variable aleatoria de la distribución normal estándar a partir de una distribución binomial: $Z = \frac{X - n\theta}{\sqrt{n\theta (1 - \theta)}}$
\end{itemize}

\part{Estadística}

\chapter{Estimación puntual}

\section{Muestras}

\begin{itemize}
  \item Media muestral: $\overline{x} = \frac{1}{n} \sum\limits_{i = 1}^n x_i$
  \item Proporción muestral: $\hat{p} = \frac{x}{n}$
  \item Varianza muestral: $S^2 = \frac{1}{n - 1} \sum\limits_{i = 1}^n (x_i - \overline{x})^2$
  \item Error estándar de la media: $\sigma_{\overline{x}} = \frac{\sigma}{\sqrt{n}}$
  \item Error estándar de la proporción: $\sigma_{\hat{p}} = \sqrt{\frac{pq}{n}}$
  \item Error estándar de la diferencia de medias: $\sigma_{\overline{x_1} - \overline{x_2}} = \sqrt{\frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}}$
  \item Error estándar de la diferencia de proporciones: $\sigma_{\hat{p_1} - \hat{p_2}} = \sqrt{\frac{p_1 q_1}{n_1} + \frac{p_2 q_2}{n_2}}$
  \item Margen de error: $MDE = \pm valor\ critico * \sigma_{\overline{x}}$
  \item Varianza de la media muestral: $var(\overline{x}) = \frac{\sigma^2}{n}$
  \item Variable aleatoria con distribución ji cuadrada: $\frac{(n - 1) S^2}{\sigma^2} \sim \chi_{n - 1}^2$
  \item R-ésimo momento muestral: $m_r' = \frac{1}{n} \sum\limits_{i = 1}^n x_i^r$
\end{itemize}

\section{Insesgadez}

\begin{itemize}
  \item Estimador insesgado: $E(\hat\theta) = \theta$
  \item Sesgo: $b(\theta) = E(\hat\theta) - \theta$
  \item Error cuadrático medio: $ECM(\hat\theta) = E[(\hat\theta - \theta)^2]$
  \item Estimador asintóticamente insesgado: $\lim\limits_{n \to \infty} b(\theta) = 0$
  \item Insesgadez de la media muestral: $E(\overline{x}) = \mu$
  \item Insesgadez de la proporción muestral: $E(\hat{p}) = p$
  \item Insesgadez de la varianza muestral: $E(S^2) = \sigma^2$
\end{itemize}

\section{Eficiencia}

\begin{itemize}
  \item Cota de Cramer-Rao: $var(\hat\theta) \ge \frac{1}{n E\left[\left(\frac{\partial \ln f(x)}{\partial \theta} \right)^2 \right]}$
  \item Estimador de mínima varianza: $var(\hat\theta) = \frac{1}{n E\left[\left(\frac{\partial \ln f(x)}{\partial \theta} \right)^2 \right]}$
  \item Eficiencia de $\hat\theta_1$ respecto a $\hat\theta_2$: $eff(\hat\theta_1, \hat\theta_2) = \frac{var(\hat\theta_1)}{var(\hat\theta_2)}$
\end{itemize}

\section{Consistencia}

\begin{itemize}
  \item Estimador consisente: $\lim\limits_{n \to \infty} P(|\hat\theta - \theta| < c) = 1\ \forall c > 0$
  \item Condición de consistencia: $\mathrm{plim}_{n \to \infty} = \theta \iff \lim\limits_{n \to \infty} var(\hat\theta) = 0$
\end{itemize}

\section{Independencia}

\begin{itemize}
  \item Estimador independiente: $f(x_1, ..., x_n | \hat\theta) \perp \theta$
  \item Criterio de factorización de Fisher-Neyman: $f(x_1, ..., x_n | \hat\theta) \perp \theta \iff f(x_1, ..., x_n; \theta) = g(\hat\theta, \theta) \cdot h(x_1, ..., x_n)$, $h(x_1, ..., x_n) \perp \theta$
\end{itemize}

\section{Máxima verosimilitud}

\begin{itemize}
  \item Función de verosimilitud: $L(\theta) = f(x_1, ..., x_n; \theta) = \prod\limits_{i = 1}^n f(x_i)$
  \item Estimador de máxima verosimilitud: $\hat\theta_{mv} = \arg \max\limits_{\theta \in \Theta} L(\theta)$
\end{itemize}

\chapter{Estimación por intervalos}

\begin{itemize}
  \item Coeficiente de confianza: $\lambda = 1 - \alpha$
  \item Intervalo de confianza: $P(\hat\theta_1 < \theta < \hat\theta_2) = 1 - \alpha$
\end{itemize}

\section{Estimación de medias}

\begin{itemize}
  \item Teorema del límite central: $Z = \frac{\overline{x} - \mu}{\sigma / \sqrt{n}} \implies Z \sim N(0, 1)$
  \item Intervalo de confianza para la media: \\
  $\overline{x} - \frac{\sigma}{\sqrt{n}} z_{\alpha / 2} < \mu < \overline{x} + \frac{\sigma}{\sqrt{n}} z_{\alpha / 2}$
  \item Distribución $t$ de Student para la media: $\frac{\overline{x} - \mu}{S / \sqrt{n}} \sim t_{n - 1, \alpha / 2}$
  \item Intervalo de confianza para la media, con $\sigma^2$ desconocida: \\
  $\overline{x} - \frac{S}{\sqrt{n}} t_{n - 1, \alpha / 2} < \mu < \overline{x} + \frac{S}{\sqrt{n}} t_{n - 1, \alpha / 2}$
\end{itemize}

\section{Estimación de proporciones}

\begin{itemize}
  \item Distribución normal estándar para la proporción: \\
  $Z = \frac{\hat{p} - p}{\sqrt{\frac{\hat{p} (1 - \hat{p})}{n}}} = \frac{\hat{p} - p}{\sqrt{\frac{\hat{p} \hat{q}}{n}}} \implies Z \sim N(0, 1)$
  \item Intervalo de confianza para la proporción: \\
  $\hat{p} - Z_{\alpha / 2} \sqrt{\frac{\hat{p}\hat{q}}{n}} < p < \hat{p} + Z_{\alpha / 2} \sqrt{\frac{\hat{p}\hat{q}}{n}}$
\end{itemize}

\section{Estimación de la varianza}

\begin{itemize}
  \item Intervalo de confianza para la distribución ji cuadrada: \\
  $P(\chi_{n - 1; 1 - \alpha / 2}^2 < \chi_{n - 1}^2 < \chi_{n - 1; \alpha / 2}^2) = 1 - \alpha$
  \item Intervalo de confianza para la varianza: \\
  $\frac{(n - 1) S^2}{\chi_{n - 1; \alpha / 2}^2} < \sigma^2 < \frac{(n - 1) S^2}{\chi_{n - 1; 1 - \alpha / 2}^2}$
\end{itemize}

\section{Estimación de la diferencia de medias}

\begin{itemize}
  \item Distribución normal estándar para la diferencia de medias, con varianza conocida: \\
  $Z = \frac{\overline{x_1} - \overline{x_2} - (\mu_1 - \mu_2)}{\sqrt{\frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}}} \implies Z \sim N(0,1)$
  \item Intervalo de confianza para la diferencia de medias, con varianza conocida: $(\overline{x_1} - \overline{x_2}) - Z_{\alpha / 2} \sqrt{\frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}} < \mu_1 - \mu_2 < (\overline{x_1} - \overline{x_2}) + Z_{\alpha / 2} \sqrt{\frac{\sigma_1^2}{n_1} + \frac{\sigma_2^2}{n_2}}$
¸\item Varianza ponderada: $S_p^2 = \frac{(n_1 - 1)S_1^2 + (n_2 - 1)S_2^2}{n_1 + n_2 - 2}$
  \item Distribución $t$ de Student para la diferencia de medias, con la misma varianza desconocida: $T = \frac{\overline{x_1} - \overline{x_2} - (\mu_1 - \mu_2)}{S_p \sqrt{\frac{1}{n_1} + \frac{1}{n_2}}} \implies T \sim t_{n_1 + n_2 - 2, \alpha / 2}$
  \item Intervalo de confianza para la diferencia de medias, con la misma varianza desconocida: \\
  $(\overline{x_1} - \overline{x_2}) - t_{n_1 + n_2 - 2, \alpha / 2} S_p \sqrt{\frac{1}{n_1} + \frac{1}{n_2}} < \mu_1 - \mu_2 \\ < (\overline{x_1} - \overline{x_2}) + t_{n_1 + n_2 - 2, \alpha / 2} S_p \sqrt{\frac{1}{n_1} + \frac{1}{n_2}}$
  \item Grados de libertad de la distribución $t$ de Student para la diferencia de medias, con varianzas desconocidas: $\nu = \frac{\left(\frac{S_1^2}{n_1} + \frac{S_2^2}{n_2}\right)^2}{\frac{(S_1^2 / n_1)^2}{n_1 - 1} + \frac{(S_2^2 / n_2)^2}{n_2 - 1}}$
  \item Intervalo de confianza para la diferencia de medias, con varianzas desconocidas: $(\overline{x_1} - \overline{x_2}) - t_{\alpha / 2, \nu} \sqrt{\frac{S_1^2}{n_1} + \frac{S_2^2}{n_2}} < \mu_1 - \mu_2 < (\overline{x_1} - \overline{x_2}) + t_{\alpha / 2, \nu} \sqrt{\frac{S_1^2}{n_1} + \frac{S_2^2}{n_2}}$
\end{itemize}

\section{Estimación de la diferencia de proporciones}

\begin{itemize}
  \item Intervalo de confianza para la diferencia de proporciones: \\
  $(\hat\theta_1 - \hat\theta_2) - z_{\alpha / 2} \sqrt{\frac{\hat\theta_1 (1 - \hat\theta_1)}{n_1} + \frac{\hat\theta_2 (1 - \hat\theta_2)}{n_2}} < \theta_1 - \theta_2 < (\hat\theta_1 - \hat\theta_2) + z_{\alpha / 2} \sqrt{\frac{\hat\theta_1 (1 - \hat\theta_1)}{n_1} + \frac{\hat\theta_2 (1 - \hat\theta_2)}{n_2}}$
\end{itemize}

\section{Estimación de la razón de varianzas}

\begin{itemize}
  \item Intervalo de confianza para la razón de dos varianzas: \\
  $\frac{S_1^2}{S_2^2} \cdot \frac{1}{f_{\alpha / 2, n_1 - 1, n_2 - 1}} < \frac{\sigma_1^2}{\sigma_2^2} < \frac{S_1^2}{S_2^2} \cdot f_{\alpha / 2, n_2 - 1, n_1 - 1}$
\end{itemize}

\end{document}          
